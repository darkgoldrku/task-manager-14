package ru.t1.bugakov.tm.api.Command;

public interface ICommandController {
    void showInfo();

    void showAbout();

    void showVersion();

    void showHelp();

    void showCommands();

    void showArguments();

    void showArgumentError();

    void showCommandError();
}
