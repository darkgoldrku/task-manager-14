package ru.t1.bugakov.tm.api.Comparator;

public interface IHasName {

    String getName();

    void setName(String name);

}
