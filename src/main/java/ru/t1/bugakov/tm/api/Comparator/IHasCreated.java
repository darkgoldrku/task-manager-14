package ru.t1.bugakov.tm.api.Comparator;

import java.util.Date;

public interface IHasCreated {

    Date getCreated();

    void setCreated(Date created);

}
